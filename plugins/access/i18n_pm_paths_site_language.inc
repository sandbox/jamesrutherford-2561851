<?php

/**
 * @file
 * Plugin to provide access control based upon node type.
 */

// Define the access plugin.
if (module_exists('locale')) {
  $plugin = array(
    'title' => t('i18n PM Paths - User: language'),
    'description' => t('=Control access by the language and path the user is currently on.'),
    'callback' => 'i18n_pm_paths_ctools_access_check',
    'default' => array('language' => array()),
    'settings form' => 'i18n_pm_paths_ctools_access_settings',
    'settings form submit' => 'i18n_pm_paths_ctools_access_settings_submit',
    'summary' => 'i18n_pm_paths_ctools_access_summary',
  );
}

/**
 * Settings form for the 'by TWC site_language' access plugin.
 */
function i18n_pm_paths_ctools_access_settings($form, &$form_state, $conf) {
  $path = current_path();

  $options = array(
    'default' => t('i18n PM Paths Default site language'),
  );
  $options = array_merge($options, locale_language_list());
  $form['settings']['language'] = array(
    '#title' => t('i18n PM Paths Language'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#description' => t('i18n PM Paths Pass only if the current site language is one of the selected languages.'),
    '#default_value' => $conf['language'],
  );

  $form['settings']['page_conf'] = array(
    '#type'  => 'hidden',
    '#value' => $path,
  );

  return $form;
}

/**
 * Normalizes a path for later comparison.
 *
 * @param string $path
 *   The string to normalize.
 *
 * @return array
 *   An array of path parts with all space trimmed and duplicate slashes
 *   removed.
 */
function _i18n_pm_paths_ctools_access_normalize_path($path) {
  return array_filter(explode('/', trim($path)));
}

/**
 * Checks for access.
 */
function i18n_pm_paths_ctools_access_check($conf, $context) {

  // Make sure the current language is allowed in this variant.
  global $language;
  $current_language  = $language->language;
  $allowed_languages = array_filter($conf['language']);
  if (!in_array($current_language, $allowed_languages)) {
    return FALSE;
  }

  // The ctools path saved will be either "add" (for new settings) or
  // "configure" for existing selection rules being adjusted. Either way we
  // parse them for the actual ctools system page name as there is no other
  // api call to get it.
  //
  // Get ctools path saved during form submission.
  $path_parts = explode('*', $conf['page_conf']);

  // Manually scrape string for page manager page name.
  //
  // @TODO For cleaner approach, choose one of these options:
  // - Use regex to grab the page name, or...
  // - Explode on slash and manipulate the 6th element (index 5), or...
  // - Use strpos('*') and strrpos('-') to isolate the page name.
  if (strpos($path_parts[0], 'access/configure') !== FALSE) {
    $page_name = str_replace('ctools/context/ajax/access/configure/page_manager_task_handler-page-', '', $path_parts[0]);
  }
  else {
    $page_name = str_replace('ctools/context/ajax/access/add/page_manager_task_handler-page-', '', $path_parts[0]);
  }

  // Get the page definition from the page manager subtasks.
  $task = page_manager_get_task('page');
  $page = page_manager_page_subtask($task, $page_name);

  // Grab the i18n path for the current language. If it is empty, fall back to
  // the page (subtask) path.
  $i18n_aliases = $page['subtask']->conf['i18n_aliases'];
  $i18n_key     = 'i18n_pm_paths_' . $current_language;
  $i18n_path    = empty($i18n_aliases[$i18n_key])
    ? $page['subtask']->path
    : $i18n_aliases[$i18n_key];

  // Normalize the paths and break them into path parts.
  $current_path = _i18n_pm_paths_ctools_access_normalize_path(current_path());
  $i18n_path    = _i18n_pm_paths_ctools_access_normalize_path($i18n_path);

  // Remove tokens from current path and i18n path for later comparison.
  foreach ($i18n_path as $index => $path_part) {
    switch (substr($path_part, 0, 1)) {
      case '#':
      case '!':
        unset($current_path[$index],
                 $i18n_path[$index]);
        break;
    }
  }

  // With all arguments removed, does the current path match the i18n path?
  //
  // Return FALSE to fall down to the next variant. Note that there was a
  // discussion about responding with 404 when the requested path does not match
  // the i18n path for the request language.
  return $current_path === $i18n_path;
}

/**
 * Provide a summary description based upon the checked site_languages.
 */
function i18n_pm_paths_ctools_access_summary($conf, $context) {
  $languages = array(
    'default' => t('i18n PM Paths Default site language'),
  );
  $languages = array_merge($languages, locale_language_list());

  if (!isset($conf['language'])) {
    $conf['language'] = array();
  }

  $names = array();
  foreach (array_filter($conf['language']) as $language) {
    $names[] = $languages[$language];
  }

  if (empty($names)) {
    return t('i18n PM Paths Site language is any language');
  }

  return format_plural(count($names), 'i18n PM Paths Site language is "@languages"', 'Site language is one of "@languages"', array('@languages' => implode(', ', $names)));
}
